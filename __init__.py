# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool


def register():
    Pool.register(
        module='timesheet_work_notebook', type_='model')
    Pool.register(
        module='timesheet_work_notebook', type_='wizard')
    Pool.register(
        module='timesheet_work_notebook', type_='report')
